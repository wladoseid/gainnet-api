<?php

namespace Gainnet\API\Client;

use Gainnet\API\HttpClient as BaseHttpClient;

class HttpClient extends BaseHttpClient
{
    const URL_BALANCE = 'api/v1/balance';
    const URL_SET_PRICE = 'api/v1/price';
    const URL_LEADS = 'api/v1/leads';
    const URL_UPDATE = 'api/v1/update';

    protected $apiKey = null;

    /**
     * @param string|null $apiKey
     *
     * @return \Gainnet\API\Client\HttpClient
     */
    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @param int $leadId
     * @param int $leadStatus
     * @param string|null $reason
     * @param string|null $urlUpdate
     *
     * @return bool
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function update(int $leadId, int $leadStatus, $reason = null, $urlUpdate = null): bool
    {
        $this->send(
            self::METHOD_POST,
            $urlUpdate ?: self::URL_UPDATE,
            array_filter([
                'api_key' => $this->apiKey,
                'lead_status' => $leadStatus,
                'lead_id' => $leadId,
                'reason' => $reason,
            ])
        );

        return true;
    }

    /**
     * @param int|null $leadStatus
     * @param string|null $fromDate
     * @param string|null $toDate
     * @param int $limit
     * @param string|null $urlLeads
     *
     * @return mixed
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function leads($leadStatus = null, $fromDate = null, $toDate = null, int $limit = 100, $urlLeads = null)
    {
        return $this->send(
            self::METHOD_POST,
            $urlLeads ?: self::URL_LEADS,
            array_filter([
                'api_key' => $this->apiKey,
                'lead_status' => $leadStatus,
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'limit' => $limit,
            ])
        )->getJson('answer');
    }

    /**
     * @param string|null $urlBalance
     *
     * @return int|float
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function balance($urlBalance = null)
    {
        $response = $this->send(self::METHOD_POST, $urlBalance ?: self::URL_BALANCE, ['api_key' => $this->apiKey]);

        return (float)$response->getJson('message');
    }

    /**
     * @param int $leadId
     * @param int $price
     * @param string|null $urlPrice
     *
     * @return bool
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function setPrice(int $leadId, int $price, $urlPrice = null): bool
    {
        $this->send(
            self::METHOD_POST,
            $urlPrice ?: self::URL_SET_PRICE,
            [
                'api_key' => $this->apiKey,
                'price' => $price,
                'lead_id' => $leadId,
            ]
        );

        return true;
    }
}