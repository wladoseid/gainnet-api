<?php

namespace Gainnet\API\Webmaster;

use Gainnet\API\HttpClient as BaseHttpClient;

class HttpClient extends BaseHttpClient
{
    const URL_ADD_LEAD = 'api/v1/addlead';

    /**
     * @param \Gainnet\API\Webmaster\AddLeadObject $object
     * @param string|null $urlAddLead
     *
     * @return bool
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function addLeadFromObject(AddLeadObject $object, $urlAddLead = null): bool
    {
        $this->addLeadFromArray($object->toArray(), $urlAddLead);

        return true;
    }

    /**
     * @param array $data
     * @param string|null $urlAddLead
     *
     * @return bool
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    public function addLeadFromArray(array $data, $urlAddLead = null): bool
    {
        $this->send(self::METHOD_POST, $urlAddLead ?: self::URL_ADD_LEAD, $data);

        return true;
    }
}