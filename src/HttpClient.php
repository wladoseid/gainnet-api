<?php

namespace Gainnet\API;

abstract class HttpClient
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    protected $host;
    protected $beforeSendFunction = null;
    protected $afterSendFunction = null;
    protected $response = null;

    /**
     * @param string $host
     */
    public function __construct(string $host)
    {
        $this->host = $host;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     *
     * @return \Gainnet\API\Response
     * @throws \Gainnet\API\Exceptions\GainnetHttpException
     */
    protected function send(string $method, string $url, array $data): Response
    {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        if ($method === self::METHOD_POST) {
            curl_setopt($curl, CURLOPT_URL, $fullUrl = $this->makeUrl($url));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        } else {
            curl_setopt($curl, CURLOPT_URL, $fullUrl = $this->makeQueryUrl($url, $data));
        }
        if ($this->beforeSendFunction) {
            ($this->beforeSendFunction)($method, $fullUrl, $data);
        }

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $response = $this->makeResponse($curl);

        curl_close($curl);
        if ($this->afterSendFunction) {
            ($this->afterSendFunction)($method, $url, $data, $response);
        }
        $this->response = $response;
        $response->throw();

        return $response;
    }

    /**
     * @param $curl
     *
     * @return \Gainnet\API\Response
     */
    protected function makeResponse($curl): Response
    {
        $response = new Response();
        $response->setContent(curl_exec($curl));
        $response->setCode(curl_getinfo($curl, CURLINFO_RESPONSE_CODE));
        $response->setContentType(curl_getinfo($curl, CURLINFO_CONTENT_TYPE));

        return $response;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function makeUrl(string $url): string
    {
        if (strpos(strtolower($url), 'http') !== false) {
            return $url;
        }

        return trim($this->host, ' \t\n\r\0\x0B /') . '/' . trim($url, ' \t\n\r\0\x0B /');
    }

    /**
     * @param string $url
     * @param array $data
     *
     * @return string
     */
    protected function makeQueryUrl(string $url, array $data): string
    {
        $query = '';
        $url = $this->makeUrl($url);
        if (strpos($url, '?') === false) {
            $query .= '?';
        } else {
            $query .= '&';
        }

        return $url . $query . http_build_query($data);
    }

    /**
     * @param \Closure $beforeSendFunction
     *
     * @return \Gainnet\API\HttpClient
     */
    public function setBeforeSendFunction(\Closure $beforeSendFunction): self
    {
        $this->beforeSendFunction = $beforeSendFunction;

        return $this;
    }

    /**
     * @param \Closure $afterSendFunction
     *
     * @return \Gainnet\API\HttpClient
     */
    public function setAfterSendFunction(\Closure $afterSendFunction): self
    {
        $this->afterSendFunction = $afterSendFunction;

        return $this;
    }
}