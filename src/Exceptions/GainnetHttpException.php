<?php
namespace Gainnet\API\Exceptions;

use Exception;
use Gainnet\API\Response;
use Throwable;

class GainnetHttpException extends Exception
{
    const SERVER_ERROR = 100;
    const RESPONSE_DECODE_ERROR = 101;
    const RESPONSE_ERROR = 200;

    protected $response;

    /**
     * @param \Gainnet\API\Response $response
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(Response $response, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return \Gainnet\API\Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }
}