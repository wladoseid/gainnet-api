<?php

namespace Gainnet\API;

use Gainnet\API\Exceptions\GainnetHttpException;

class Response
{
    protected $code;
    protected $content;
    protected $contentType;

    /**
     * @param mixed $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @param mixed $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @param string|null $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string|null
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @return mixed|null
     */
    public function getJson($key = null)
    {
        if ($this->content) {
            $json = json_decode($this->content, true) ?: null;
            if ($json) {
                if ($key !== null) {
                    return $json[$key] ?? null;
                }

                return $json;
            }
        }

        return null;
    }

    /**
     * @throws GainnetHttpException
     */
    public function throw()
    {
        if ($this->code > 300 || $this->code < 200) {
            throw new GainnetHttpException($this, 'Server error', GainnetHttpException::SERVER_ERROR);
        }
        if (!($json = $this->getJson()) || !array_key_exists('status', $json)) {
            throw new GainnetHttpException($this, 'Content error', GainnetHttpException::RESPONSE_DECODE_ERROR);
        }
        if ($json['status'] === false || $json['status'] === 'rejected') {
            throw new GainnetHttpException($this, 'Operation fail: ' . isset($json['message']) ? $json['message'] : 'Message not send.', GainnetHttpException::RESPONSE_ERROR);
        }
    }
}