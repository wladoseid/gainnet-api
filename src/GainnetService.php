<?php

namespace Gainnet\API;

class GainnetService
{
    protected $gainnetHost = 'https://gainnet.ru';
    protected $onenetHost = 'https://onenet.ru';

    /**
     * @param string|null $gainnetHost
     *
     * @return \Gainnet\API\GainnetService
     */
    public function setGainnetHost($gainnetHost): self
    {
        $this->gainnetHost = $gainnetHost;

        return $this;
    }

    /**
     * @param string|null $onenetHost
     *
     * @return \Gainnet\API\GainnetService
     */
    public function setOnenetHost($onenetHost): self
    {
        $this->onenetHost = $onenetHost;

        return $this;
    }

    /**
     * @return \Gainnet\API\Webmaster\HttpClient
     */
    public function webmaster(): Webmaster\HttpClient
    {
        return new Webmaster\HttpClient($this->onenetHost);
    }

    /**
     * @return \Gainnet\API\Client\HttpClient
     */
    public function client(): Client\HttpClient
    {
        return new Client\HttpClient($this->gainnetHost);
    }
}