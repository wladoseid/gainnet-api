# Gainnet API
Разработал Wladoseid(wladoseid@gmail.com) для компании ООО "Центр интернет заявок"

## Установка
Выполнить команду:
```SH
composer require wladoseid/gainnet-api:1.0.8
```

## Использование для вебмастеров

Получение инстанца сервиса:
```PHP
$gainnet = new \Gainnet\API\GainnetService();
$httpClient = $gainnet->webmaster();
```
Если Вам необходмо указать особый хост:
```PHP
$gainnet = new \Gainnet\API\GainnetService();
$httpClient = $gainnet->setOnenetHost('https://onenet.ru')->webmaster();
```
Для добавления лида необходимо вызвать одну из функций:
1) Для передачи объекта
```PHP
// Заполняем объект
$addLeadObject = new \Gainnet\API\Webmaster\AddLeadObject();
$addLeadObject->setId(1); // Иднтификатор (API) вашей учетной записи
$addLeadObject->setName('Борис Грачевский');
$addLeadObject->setPhone('+7 987 654-32-10');
$addLeadObject->setText('Как разделить квартиру на троих?');
$addLeadObject->setSource('https://my.site');
// Передаем объект в функцию,
// используйте try {} cache() {}
// так как в случае ошибки со стороны сервера будет выбрашена ошибка
try {
    $httpClient->addLeadFromObject($addLeadObject);
} catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
// в случае успеха вернется true
```
2) Для передачи массива
```PHP
// Заполняем объект
$addLeadArray = [
    'id' => 1, // Иднтификатор (API) вашей учетной записи
    'name' => 'Борис Грачевский',
    'phone' => '+7 987 654-32-10',
    'text' => 'Как разделить квартиру на троих?',
    'source' => 'https://my.site',
];
// Передаем массив в функцию,
// используйте try {} cache() {}
// так как в случае ошибки со стороны сервера будет выбрашена ошибка
try {
    $httpClient->addLeadFromArray($addLeadArray);
} catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
// в случае успеха вернется true
```
## Использование для участников аукциона

Получение инстанца сервиса:
```PHP
$gainnet = new \Gainnet\API\GainnetService();
$httpClient = $gainnet->client()->setApiKey('...');
```
Если Вам необходмо указать особый хост:
```PHP
$gainnet = new \Gainnet\API\GainnetService();
$httpClient = $gainnet->setGainnetHost('https://gainnet.ru')->client()->setApiKey('...');
```
### Функции
1) Получение баланса
```PHP
try {
    $balance = $httpClient->balance();
}  catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
```
2) Установить цену [demo]
```PHP
try {
    $httpClient->setPrice(
        1, // идентификатор лида
        100 // цена (целое число) 
    );
}  catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
// В случае успеха вернет true
```
3) Получение списка лидов
```PHP
try {
    $leads = $httpClient->leads(
        100, // Статус лидов [опционально]
        '2021-10-10 22:10:15', // Дата от какого числа [опционально]
        '2021-10-11 22:10:15', // Дата до какого числа [опционально]
        50 // лимит записей [опционально]
    );
}  catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
```
Ответ:
```PHP
array(1) {
  [0]=>
  array(12) {
    ["id"]=>
    int(123456)
    ["phone"]=>
    string(11) "79998765432"
    ["name"]=>
    string(14) "Татьяна"
    ["text"]=>
    string(117) "Как оплатить долг по капитальному ремонту?"
    ["sold_price"]=>
    int(160)
    ["reason"]=>
    NULL
    ["status"]=>
    string(17) "Новый лид"
    ["type"]=>
    string(8) "Фикс"
    ["region"]=>
    string(18) "Краснодар"
    ["sold_date"]=>
    string(27) "2020-12-30T13:38:45.000000Z"
    ["route_show"]=>
    string(34) "https://gainnet.ru/my/lead/123456"
    ["is_test"]=>
    bool(false)
  }
}

```

4) Обновить статус лидов (возможные статусы 101 - новый лид, 102 - рабочий лил)
```PHP
try {
    $httpClient->update(
        1, // id лида
        101, // статус лида
        'Не отвечает на звонки' // причина [указывать в случае 900 статуса]
    );
}  catch (\Gainnet\API\Exceptions\GainnetHttpException $exception) {
    echo 'Error: ' . $exception->getMessage(); die();
}
// В случае успеха вернет true
```